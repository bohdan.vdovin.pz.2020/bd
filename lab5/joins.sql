USE vdovinInd

-- Get all description per dish
SELECT
	ds.name,
	ds.category,
	ds.serveware_type,
	descr.country,
	descr.creation_time,
	descr.weight
FROM Dishes AS ds
JOIN Descriptions AS descr
ON ds.ID = descr.dishID;

-- Get last price and count per dish
SELECT 
	ds.name,
	ds.category,
	ds.serveware_type,
	dsC.count,
	dsC.time,
	dsP.price,
	dsP.time
FROM Dishes AS ds
JOIN (
	SELECT
		dishID,
		count,
		time
	FROM DishCounts AS dsCI
	WHERE time IN (
		SELECT TOP(1) MAX(time)
		FROM DishCounts
		WHERE dsCI.dishID = dishID
	)
) AS dsC
ON dsC.dishID = ds.ID
JOIN (
	SELECT
		dishID,
		price,
		time
	FROM DishPrices AS dsPI
	WHERE time IN (
		SELECT TOP(1) MAX(time)
		FROM DishPrices
		WHERE dsPI.dishID = dishID
	)
) AS dsP
ON dsP.dishID = ds.ID;


--Find number of orders performed by each employee
SELECT empl.ID, COUNT(ord.ID) AS count
FROM Employees AS empl
LEFT OUTER JOIN Orders AS ord
ON ord.employeeID = empl.ID
GROUP BY empl.ID;

--Find price of orders performed by each employee
SELECT
	empl.ID,
	empl.firstname,
	empl.lastname,
	SUM(TP.totalPrice) as total
FROM Employees AS empl
LEFT OUTER JOIN Orders AS ord
ON ord.employeeID = empl.ID
LEFT OUTER JOIN (
	SELECT
		orderID,
		SUM(dsC.count*dsP.price) AS totalPrice
	FROM OrderToDishes AS OTD
	JOIN DishCounts AS dsC
	ON dsC.dishID = OTD.dishID
	JOIN DishPrices AS dsP
	ON dsP.dishID = OTD.dishID
	GROUP BY OTD.orderId
) AS TP
ON ord.ID = TP.orderId 
GROUP BY empl.ID, empl.firstname, empl.lastname;

--Find all materials per dish
SELECT
	ds.ID,
	ds.name,
	STRING_AGG(CONVERT(NVARCHAR(max), ISNULL(dsM.material,'N/A')), ',') AS materiaslList
FROM Dishes AS ds
JOIN DishMaterials AS dsM
ON dsM.dishID = ds.ID
GROUP BY ds.ID, ds.name;

--Get discouts per registered customer
SELECT
	cs.ID,
	cs.firstname,
	cs.lastname,
	dc.discount
FROM Customers AS cs
JOIN DiscountCards AS dc
ON dc.customerID = cs.ID;

--Get total price and count for all selled dishes per year
SELECT
	YEAR(ord.date) AS year,
	SUM(TP.totalPrice) AS total,
	COUNT(ord.ID) AS count
FROM Orders AS ord
LEFT OUTER JOIN (
	SELECT
		orderID,
		SUM(dsC.count*dsP.price) AS totalPrice
	FROM OrderToDishes AS OTD
	JOIN DishCounts AS dsC
	ON dsC.dishID = OTD.dishID
	JOIN DishPrices AS dsP
	ON dsP.dishID = OTD.dishID
	GROUP BY OTD.orderId
) AS TP
ON ord.ID = TP.orderId 
GROUP BY YEAR(ord.date);
