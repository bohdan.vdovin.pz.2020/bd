USE vdovinInd
CREATE TABLE Allinfo (
	DIID INT,
    DIname NVARCHAR(50) NOT NULL,
    DIcategory NVARCHAR(50) NOT NULL,
    DIserveware_type NVARCHAR(50) NOT NULL,

    DPdishID INT,
    DPprice FLOAT NOT NULL,
    DPtime DATETIME NOT NULL,

	DCdishID INT,
    DCcount INT NOT NULL,
    DCtime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,

	DSdishID INT,
    DScountry text,
    DScreation_time DATE NOT NULL,
    DSweight FLOAT,

	DMdishID INT,
	DMmaterial NVARCHAR(50) NOT NULL,

	EMID INT,
    EMfirstname NVARCHAR(50) NOT NULL,
	EMlastname NVARCHAR(50) NOT NULL,
    EMemail NVARCHAR(320) NOT NULL,
    EMpassword_hash TEXT NOT NULL,
    EMage INT NOT NULL,
    EMphone NVARCHAR(20) NOT NULL,
    EMstarted_working DATE NOT NULL,
	EMsalary FLOAT NOT NULL,

    CUID INT,
    CUfirstname NVARCHAR(50) NOT NULL,
	CUlastname NVARCHAR(50) NOT NULL,
    CUemail NVARCHAR(50) NULL,
    CUphone NVARCHAR(20) NOT NULL,

	DISCID INT,
    DISCcustomerID INT,
    DISCdiscount FLOAT NOT NULL,

	ORDID INT,
    ORDemployeeID INT NOT NULL,
    ORDdate DATE NOT NULL,
    ORDdiscount_cardID INT NULL,

	ORDTDIorderId INT,
    ORDTDIdishID INT,
    ORDTDIcount INT NOT NULL
);