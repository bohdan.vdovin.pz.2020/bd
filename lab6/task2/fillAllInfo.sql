USE vdovinInd
SELECT
	DI.ID as DIID,
	DI.name as DIname,
    DI.category as DIcategory,
    DI.serveware_type as DIserveware_type,

    DP.dishID as DPdishID,
    DP.price as DPprice,
    DP.time as DPtime,

    DC.dishID as DCdishID,
    DC.count as DCcount,
    DC.time as DCtime,

    DS.dishID as DSdishID,
    DS.country as DScountry,
    DS.creation_time as DScreation_time,
    DS.weight as DSweight,

    DM.dishID as DMdishID,
    DM.material as DMmaterial,

    ORDTDI.orderID as ORDTDIorderId,
    ORDTDI.dishID as ORDTDIdishID,
    ORDTDI.count as ORDTDIcount,

    ORD.ID as ORDID,
	ORD.employeeID as ORDemployeeeID,
	ORD.date as ORDdate,
	ORD.discount_cardID as ORDdiscount_cardID,

	EM.ID as EMID,
	EM.firstname as EMfirstname,
	EM.lastname as EMlastname,
	EM.email as EMemail,
	EM.password_hash as EMpassword_hash,
	EM.age as EMage,
	EM.phone as EMphone,
	EM.started_working as EMstarted_working,
	EM.salary as EMsalary,

	DISC.ID as DISCID,
	DISC.customerID as DISCcustomerID,
	DISC.discount as DISCdiscount,

	CU.ID as CUID,
	CU.firstname as CUfirstname,
	CU.lastname as CUlastname,
	CU.email as CUemail,
	CU.phone as CUphone

	INTO Allinfo2
	FROM Dishes DI

	FULL JOIN DishPrices DP
	ON DI.ID = DP.dishID

    FULL JOIN DishCounts DC
    ON DI.ID = DC.dishID

    FULL JOIN Descriptions DS
    ON DI.ID = DS.dishID

    FULL JOIN DishMaterials DM
    ON DI.ID = DM.dishID

    FULL JOIN OrderToDishes ORDTDI
    ON ORDTDI.dishID = DI.ID

    FULL JOIN Orders ORD
    ON ORDTDI.orderID = ORD.ID

    FULL JOIN Employees EM
    ON ORD.employeeID = EM.ID

	FULL JOIN DiscountCards as DISC
	ON DISC.ID = ORD.discount_cardID

	FULL JOIN Customers as CU
	ON CU.ID = DISC.customerID
;