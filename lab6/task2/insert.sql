declare
	@DIID INT,
    @DIname NVARCHAR(50) ,
    @DIcategory NVARCHAR(50) ,
    @DIserveware_type NVARCHAR(50),

    @DPdishID INT,
    @DPprice FLOAT ,
    @DPtime DATETIME ,

	@DCdishID INT,
    @DCcount INT,
    @DCtime DATETIME,

	@DSdishID INT,
    @DScountry NVARCHAR(100),
    @DScreation_time DATE,
    @DSweight FLOAT,

	@DMdishID INT,
	@DMmaterial NVARCHAR(50),

	@EMID INT,
    @EMfirstname NVARCHAR(50),
	@EMlastname NVARCHAR(50),
    @EMemail NVARCHAR(320),
    @EMpassword_hash NVARCHAR(100),
    @EMage INT,
    @EMphone NVARCHAR(20),
    @EMstarted_working DATE,
	@EMsalary FLOAT,

    @CUID INT,
    @CUfirstname NVARCHAR(50),
	@CUlastname NVARCHAR(50),
    @CUemail NVARCHAR(50),
    @CUphone NVARCHAR(20),

	@DISCID INT,
    @DISCcustomerID INT,
    @DISCdiscount FLOAT,

	@ORDID INT,
    @ORDemployeeeID INT,
    @ORDdate DATE,
    @ORDdiscount_cardID INT,

	@ORDTDIorderId INT,
    @ORDTDIdishID INT,
    @ORDTDIcount INT

declare allinfo cursor local for
	select
		DIID, DIname, DIcategory, DIserveware_type,
        DPdishID, DPprice, DPtime,
        DCdishID, DCcount, DCtime,
        DSdishID, DScountry, DScreation_time, DSweight,
        DMdishID, DMmaterial,
        EMID, EMfirstname, EMlastname, EMemail, EMpassword_hash, EMage, EMphone, EMstarted_working, EMsalary,
        CUID, CUfirstname, CUlastname, CUemail, CUphone,
        DISCID,DISCcustomerID, DISCdiscount,
        ORDID, ORDemployeeeID, ORDdate, ORDdiscount_cardID,
        ORDTDIorderId, ORDTDIdishID, ORDTDIcount
		
		from AllInfo2

open allinfo

declare
	@dishID int,
	@customerID int,
	@employeeID int,
	@orderID int

set @dishID = 0
set @employeeID = 0
set @orderID = 0
set @customerID = 0

fetch next from allinfo into
	@DIID, @DIname, @DIcategory, @DIserveware_type,
	@DPdishID, @DPprice, @DPtime,
    @DCdishID, @DCcount, @DCtime,
	@DSdishID, @DScountry, @DScreation_time, @DSweight,
	@DMdishID, @DMmaterial,
	@EMID, @EMfirstname, @EMlastname, @EMemail, @EMpassword_hash, @EMage, @EMphone, @EMstarted_working, @EMsalary,
	@CUID, @CUfirstname, @CUlastname, @CUemail, @CUphone,
	@DISCID,@DISCcustomerID, @DISCdiscount,
	@ORDID, @ORDemployeeeID, @ORDdate, @ORDdiscount_cardID,
	@ORDTDIorderId, @ORDTDIdishID, @ORDTDIcount

while @@fetch_status = 0 begin
	-- Dishes
	if @DIID is not null and not exists (select ID from Dishes where ID = @DIID) begin
		insert into Dishes
			values (
				@DIID,
				@DIname,
				@DIcategory,
				@DIserveware_type
			)
		insert into DishPrices
			values (
				@DPdishID, @DPprice, @DPtime
			)
		insert into DishCounts
			values (
				@DCdishID, @DCcount, @DCtime
			)
		insert into Descriptions 
			values (
				@DSdishID, @DScountry, @DScreation_time, @DSweight
			)
	end
	-- Dish Materials
	if @DIID is not null and not exists (select * from DishMaterials dm where dm.dishID = @DIID and dm.material = @DMmaterial) begin
		insert into DishMaterials
			values (
				@DIID,
				@DMmaterial
			)
	end
	-- Employees
	if @EMfirstname is not null and  not exists (select ID from Employees	EM where EM.firstname = @EMfirstname and EM.lastname = @EMlastname and EM.phone = @EMphone) begin
		insert into Employees
			values (
				@employeeID, @EMfirstname, @EMlastname, @EMemail, @EMpassword_hash, @EMage, @EMphone, @EMstarted_working, @EMsalary
			)
			set @employeeID = @employeeID + 1
	end
	-- Customers
	if @CUfirstname is not null and not exists (select ID from Customers CU where CU.firstname = @CUfirstname and CU.lastname = @CUlastname and CU.phone = @CUphone) begin
		insert into Customers
			values (
				@customerID, @CUfirstname, @CUlastname, @CUemail, @CUphone
			)
		
		insert into DiscountCards
			values (
				@customerID, @customerID, @DISCdiscount
			)

		set @customerID = @customerID + 1
	end
	-- Orders
	if @ORDdate is not null and not exists 
	(
		select ID from Orders as ORD
		where ORD.date = @ORDdate
			and
			ORD.employeeID IN (
				SELECT ID from Employees EM WHERE EM.firstname = @EMfirstname and EM.lastname = @EMlastname and @EMphone = @EMphone
			)
			and ORD.discount_cardID IN (
				SELECT DS.ID from DiscountCards as DS
				JOIN Customers CU
				ON CU.ID = DS.customerID
				WHERE CU.firstname = @CUfirstname and CU.lastname = @CUlastname and CU.phone = @CUphone
			)
	) begin
		INSERT INTO Orders 
			(
				ID,
				employeeID,
				date,
				discount_cardID
			)
			values (
				@orderID,
				(
					SELECT ID from Employees EM WHERE EM.firstname = @EMfirstname and EM.lastname = @EMlastname and @EMphone = @EMphone
				),
				@ORDdate,
				(
					SELECT DS.ID from DiscountCards as DS
					JOIN Customers CU
					ON CU.ID = DS.customerID
					WHERE CU.firstname = @CUfirstname and CU.lastname = @CUlastname and CU.phone = @CUphone
				)

			)
		SET @orderID = @orderID + 1
	end

	if @ORDdate is not null begin
		declare @oid int

		SELECT @oid=ID from Orders as ORD
		WHERE ORD.date = @ORDdate
			and ORD.employeeID IN
				(
					SELECT ID from Employees EM
					WHERE EM.firstname = @EMfirstname
						and EM.lastname = @EMlastname
						and @EMphone = @EMphone
				)
			and ORD.discount_cardID IN 
			(
					SELECT DS.ID from DiscountCards as DS
					JOIN Customers CU
					ON CU.ID = DS.customerID
						WHERE CU.firstname = @CUfirstname and CU.lastname = @CUlastname and CU.phone = @CUphone
		)

		INSERT INTO OrderToDishes
				(
					orderId,
					dishID,
					count
				)
				values (
					@oid,
					@DIID,
					@DCcount
				)
	end

	fetch next from allinfo into
		@DIID, @DIname, @DIcategory, @DIserveware_type,
		@DPdishID, @DPprice, @DPtime,
		@DCdishID, @DCcount, @DCtime,
		@DSdishID, @DScountry, @DScreation_time, @DSweight,
		@DMdishID, @DMmaterial,
		@EMID, @EMfirstname, @EMlastname, @EMemail, @EMpassword_hash, @EMage, @EMphone, @EMstarted_working, @EMsalary,
		@CUID, @CUfirstname, @CUlastname, @CUemail, @CUphone,
		@DISCID,@DISCcustomerID, @DISCdiscount,
		@ORDID, @ORDemployeeeID, @ORDdate, @ORDdiscount_cardID,
		@ORDTDIorderId, @ORDTDIdishID, @ORDTDIcount
end

close allinfo
deallocate allinfo