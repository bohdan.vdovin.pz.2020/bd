USE VdovinTestdb
DECLARE @maxOnum int;
SELECT @maxOnum = max(onum) + 1 FROM Orders;

with nums as (
	SELECT 1 AS value
	UNION ALL
	SELECT value + 1 AS value
	FROM nums
	WHERE nums.value <= 99	
)

INSERT INTO Orders
	SELECT
		@maxOnum + (ROW_NUMBER() OVER(ORDER BY onum)) AS onum,
		1000 AS amt,
		odate,
		cnum,
		snum
	FROM Orders AS o
		INNER JOIN nums AS n
			ON n.value < (o.amt / 1000);

UPDATE Orders
	SET amt = amt / 1000
	WHERE amt > 1000;

SELECT * FROM Orders;