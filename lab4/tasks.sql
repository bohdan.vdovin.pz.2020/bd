USE [VdovinTestdb]
-- Вправа 1. Модифікуйте запит з прикладу 1, щоб результатом було повернення лише 
-- даних про ім’я замовника та його розміщення.  
 
SELECT cname, city
FROM Customers;

-- Приклад 2. Запит до навчальної бази даних, що повертає  список усіх замовників з 
-- Лондона або Риму. 

SELECT cname, city, rating
FROM Customers
WHERE city = 'London' OR city = 'Rome';
-- OR
SELECT cname, city, rating
FROM Customers
WHERE city IN ('London', 'Rome');

 
-- Вправа 3. Модифікуйте запит з прикладу 2, так щоб результатом було повернення 
-- списку замовників розміщених за межами Лондона та із рівнем переваги понад 200. 

SELECT cname, city, rating
FROM Customers
WHERE NOT city = 'London' AND rating > 200;

-- Вправа 4. Модифікуйте запит з прикладу 2, так щоб результатом було повернення 
-- списку замовників імена, яких починаються на “Gr” . 

SELECT cname, city, rating
FROM Customers
WHERE city IN ('London', 'Rome') and cname LIKE 'Gr%';

-- Вправа 5. Модифікуйте запит з прикладу 2, так щоб результатом було повернення 
-- списку замовників у іменах, яких більше 5-ти букв. 

SELECT cname, city, rating
FROM Customers
WHERE LEN(cname) > 5;

-- Вправа 6. Модифікуйте запит з прикладу 3, так щоб результатом було повернення 
-- списку замовників, що не співпрацюють із продавцем Peel, а також із продавцями, у яких 
-- комісія перевищує 0,13 . 
 
SELECT cname, city, rating
FROM Customers
WHERE snum NOT IN
(
    SELECT snum
    FROM Salers
    WHERE sname = 'Peel' or comm > 0.13
);

-- Вправа 7. Модифікуйте запит з прикладу 3, так щоб результатом було повернення 
-- списку замовників, що не здійснювали операцію купівлі продажу після 04-09-2013. 
SELECT cname, city, rating
FROM Customers
WHERE cnum NOT IN
(
    SELECT DISTINCT cnum
    FROM Orders
    WHERE odate > '04-09-2013'
);
 
-- Вправа 8. Реалізуйте запит, результатом якого є об’єднаний список замовників із 
-- вправи 6 та вправи 7 (підказка: для об’єднання результатів двох запитів застосуйте 
-- оператор UNION).  

SELECT cname, city, rating
FROM Customers
WHERE snum NOT IN
(
    SELECT snum
    FROM Salers
    WHERE sname = 'Peel' or comm > 0.13
    UNION
    SELECT DISTINCT cnum
    FROM Orders
    WHERE odate > '04-09-2013'
);

-- Вправа 9. Спробуйте виконати запиит: 
--  
-- SELECT 
--  snum, 
--  cname, 
--  COUNT(cnum) 
-- FROM Customers 
--  
-- GROUP BY 
--  
-- snum 
-- Поясніть помилку, що виникла. 
-- Ми групуємо по snum, а в групі буде більш ніж одне сname тому виникає помилка

-- Вправа 10. Реалізуйте запит, котрий підрахує кількість операцій купівлі продажу за 
-- кожен з днів. 

SELECT odate, COUNT(onum)
FROM Orders
GROUP BY odate;

-- Вправа 11. Розширте запит із вправи 10, так щоб було враховано лише продавців із 
-- комісійними неменшими, аніж 0,12. 

SELECT odate, COUNT(onum)
FROM Orders
WHERE snum IN (
	SELECT snum
	FROM Salers
	WHERE comm > 0.12
	)
GROUP BY odate;
 
-- Вправа 12.  Модифікуйте запит з прикладу 6, так щоб при підрахунку кількості 
-- замовників були враховані лише замовники із кодом переваги вищим за 200. 
SELECT snum, COUNT(cnum) 
FROM Customers 
Where rating > 200
GROUP BY snum 
HAVING COUNT(cnum) > 2;
 
-- Вправа 13.  Реалізуйте запит, що видасть результати про сумарну суму усіх 
-- операцій купівлі продажу для кожного дня і по кожному продавцю. У результуючий набір 
-- повинні увійти лише ті дні, коли сума операцій перевищила 3000, а до розгляду було 
-- прийнято лише операції купівлі-продажу на суму вищу за 100. 

SELECT snum, odate, COUNT(onum), SUM(amt)
FROM Orders
WHERE amt > 100
GROUP BY snum, odate
HAVING SUM(amt) > 3000;
