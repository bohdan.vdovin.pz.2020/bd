-- Отримати унікальні матеріали з таблиці матеріалів
SELECT DISTINCT material
FROM DishMaterials;

-- Отримати працівників у яких імя розпочинається з великої літери А
SELECT firstname, lastname, email, age, phone, started_working, salary
FROM Employees
WHERE firstname LIKE '[A-K]%';

-- Отримати працівників у яких довжина імені більша за 10
SELECT firstname, lastname, email, age, phone, started_working, salary
FROM Employees
WHERE LEN(firstname) > 7;

-- Отримати працівників, які почали працювати після 2020 року
SELECT firstname, lastname, email, age, phone, started_working, salary
FROM Employees
WHERE started_working > '2019-01-01 00:00:00';

-- Отримати кількість працівників
SELECT COUNT(ID) as count FROM Employees;

-- Отримати кількість працівників, що почали почали працювати з 2018 року але не пізніше 2019
SELECT COUNT(ID) as count
FROM Employees
WHERE started_working BETWEEN '2018-01-01 00:00:00' AND '2019-01-01 00:00:00';

-- Отримати кількість працівників по роках і посортувати у спадаючому порядку по кількості
SELECT
YEAR(started_working) AS year,
COUNT(ID) AS c
FROM Employees
GROUP BY YEAR(started_working)
ORDER BY c;

-- Отримати кількість працівників по роках і посортувати у спадаючому порядку
-- по кількості, брати до уваги лише ті роки де кількість працівників була
-- більша 10

SELECT
YEAR(started_working) AS year,
COUNT(ID) AS c
FROM Employees
GROUP BY YEAR(started_working)
HAVING COUNT(ID) > 20
ORDER BY c;

-- Отримати кількість купівель які здійснив кожен продавець в
-- спадаючому порядку
SELECT employeeID, COUNT(ID) as c
FROM Orders 
GROUP BY employeeID
ORDER BY c;

-- Отримати покупців чия знижка більша 0.03
SELECT firstname, lastname, email, phone 
FROM Customers
WHERE ID IN (
    SELECT customerID 
    FROM DiscountCards
    WHERE discount > 0.03
);

-- Отрмати загальну кількість куплених товарів по кожному замовленню
SELECT orderId, SUM(count) as count
FROM OrderToDishes
GROUP BY orderId
ORDER BY count;

-- Отримати країни з яких виготовлені товари
SELECT DISTINCT country
FROM Descriptions;

-- Отримати покупців чия знижка більша рівна 0.05 або тих що здійснили
-- покупку в період від 2019 - 2020 
SELECT firstname, lastname, email, phone 
FROM Customers
WHERE ID IN (
    SELECT customerID 
    FROM DiscountCards
    WHERE discount > 0.03
    UNION
    SELECT d.customerID
    FROM Orders as o
    JOIN DiscountCards AS d
    ON d.ID = o.discount_cardID
    WHERE o.date BETWEEN '2019-01-01 00:00:00' AND '2020-01-01 00:00:00'
);
