CREATE TABLE Salers (
	snum int,
	sname NVARCHAR(50) NOT NULL,
	city NVARCHAR(50) NOT NULL,
	comm float NOT NULL,
	PRIMARY KEY (snum)
);

CREATE TABLE Customers (
	cnum int,
	cname NVARCHAR(50) NOT NULL,
	city NVARCHAR(50) NOT NULL,
	rating int NOT NULL
	PRIMARY KEY (cnum)
);

CREATE TABLE CustomersToSalers (
	cnum int,
	snum int,
	PRIMARY KEY(cnum, snum),
	FOREIGN KEY (cnum) REFERENCES Customers(cnum),
	FOREIGN KEY (snum) REFERENCES Salers(snum),
);

CREATE TABLE Orders (
	onum int,
	amt float NOT NULL,
	odate date NOT NULL,
	cnum int,
	snum int,
	PRIMARY KEY(onum),
	FOREIGN KEY (cnum) REFERENCES Customers(cnum),
	FOREIGN KEY (snum) REFERENCES Salers(snum),
);

CREATE TABLE Admins (
	snum int
	FOREIGN KEY (snum) REFERENCES Salers(snum)
);

CREATE VIEW vCustomers
	AS
	SELECT
		cus.cname,
		cus.city as ccity,
		cus.rating
	FROM CustomersToSalers cts
	INNER JOIN Salers sal
		ON cts.snum = sal.snum
	INNER JOIN Customers cus
		ON cts.cnum = cus.cnum
	WHERE
		sal.sname = SUSER_SNAME() and exists (
			SELECT sal2.snum FROM Salers sal2
			INNER JOIN Admins adm
				ON adm.snum = sal2.snum
			WHERE sal2.sname = sal.sname
		);