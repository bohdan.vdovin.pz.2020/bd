CREATE TABLE Dishes (
    ID INT,
    name NVARCHAR(50) NOT NULL,
    category NVARCHAR(50) NOT NULL,
    serveware_type NVARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE DishPrices(
    dishID INT,
    price FLOAT NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
    FOREIGN KEY (dishID) REFERENCES Dishes(ID)
);

CREATE TABLE DishCounts(
    dishID INT,
    count INT NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
    FOREIGN KEY (dishID) REFERENCES Dishes(ID)
);

CREATE TABLE Descriptions(
    dishID INT,
    country nvarchar(50),
    creation_time DATE NOT NULL,
    weight FLOAT,
    FOREIGN KEY (dishID) REFERENCES Dishes(ID)
);

CREATE TABLE DishMaterials(
    dishID INT,
	material NVARCHAR(50) NOT NULL,
    FOREIGN KEY (dishID) REFERENCES Dishes(ID)
);

CREATE TABLE Employees(
    ID INT,
    firstname NVARCHAR(50) NOT NULL,
	lastname NVARCHAR(50) NOT NULL,
    email NVARCHAR(320) NOT NULL,
    password_hash TEXT NOT NULL,
    age INT NOT NULL,
    phone NVARCHAR(20) NOT NULL,
    started_working DATE NOT NULL,
	salary FLOAT NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE Customers(
    ID INT,
    firstname NVARCHAR(50) NOT NULL,
	lastname NVARCHAR(50) NOT NULL,
    email NVARCHAR(50) NULL,
    phone NVARCHAR(20) NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE DiscountCards(
    ID INT,
    customerID INT,
    discount FLOAT NOT NULL,
    PRIMARY KEY(ID),
    FOREIGN KEY (customerID) REFERENCES Customers(ID)
);

CREATE TABLE Orders(
    ID INT,
    employeeID INT NOT NULL,
    date DATE NOT NULL,
    discount_cardID INT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (employeeID) REFERENCES Employees(ID),
    FOREIGN KEY (discount_cardID) REFERENCES DiscountCards(ID)
);

CREATE TABLE OrderToDishes(
    orderId INT,
    dishID INT,
    count INT NOT NULL,
    FOREIGN KEY (orderID) REFERENCES Orders(ID),
    FOREIGN KEY (dishID) REFERENCES Dishes(ID)
);


