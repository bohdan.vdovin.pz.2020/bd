﻿-- Вся інформація про посуд
CREATE VIEW DishInfo AS
SELECT
	ds.ID,
	ds.name,
	ds.category,
	ds.serveware_type,

	STRING_AGG(CONVERT(NVARCHAR(max), ISNULL(dm.material,'N/A')), ',') AS materiaslList,

	dc.count,
	dp.price,
	de.country,
	de.creation_time,
	de.weight
FROM Dishes ds
LEFT JOIN DishMaterials dm
	ON dm.dishID = ds.ID
LEFT JOIN DishCounts dc
	ON dc.dishID = ds.ID
LEFT JOIN Descriptions de
	ON de.dishID = ds.ID
LEFT JOIN DishPrices dp
	ON dp.dishID = ds.ID
GROUP BY ds.ID, ds.name, ds.category, ds.serveware_type, dc.count, dp.price, de.country, de.country,de.creation_time, de.weight;

-- Вся інформація про замовлення
CREATE VIEW OrderInfo AS
SELECT
	ord.ID as ID,
	empl.ID as employeeID,
	(empl.firstname + ' ' + empl.lastname) as employee_name,
	ord.date,
	ord.discount_cardID,
	SUM(TP.totalPrice) as total
FROM Employees AS empl
LEFT OUTER JOIN Orders AS ord
	ON ord.employeeID = empl.ID
LEFT OUTER JOIN (
	SELECT
		orderID,
		SUM(dsC.count*dsP.price) AS totalPrice
	FROM OrderToDishes AS OTD
	JOIN DishCounts AS dsC
		ON dsC.dishID = OTD.dishID
	JOIN DishPrices AS dsP
		ON dsP.dishID = OTD.dishID
	GROUP BY OTD.orderId
) AS TP
	ON ord.ID = TP.orderId 
GROUP BY empl.ID, empl.firstname, empl.lastname, ord.ID, ord.date, ord.discount_cardID;

-- Віртуальна таблиця, що містить інформацію тільки про користувача, що є автентифікованим
CREATE VIEW EmployeeALL AS
SELECT
	emp.ID,
	emp.username,
	emp.firstname,
	emp.lastname,
	emp.age,
	emp.phone,
	emp.salary,
	emp.started_working
FROM Employees emp
WHERE emp.username = SUSER_NAME();

-- Віртуальна таблиця, що містить інформацію про покупця і їхню картку тільки для адміністраторів
CREATE VIEW CustomersInfo AS
SELECT
	cu.ID,
	cu.email,
	cu.firstname,
	cu.lastname,
	cu.phone,
	dc.ID as discount_cardID,
	dc.discount
FROM Customers cu
LEFT JOIN DiscountCards dc
	ON dc.customerID = cu.ID
WHERE EXISTS (
	SELECT *
	FROM Employees emp
	JOIN RolesToEmployees rte
		ON rte.employeeID = emp.ID
	JOIN Roles r
		ON r.ID = rte.roleID
	WHERE r.name = 'admin' and username = SUSER_NAME()
);
-- Віртуальна таблиця, що містить інформацію про замовлення здійсненні даним працівником або всіх для адміністратора
CREATE VIEW OrdersPerEmployee AS
SELECT
	ord.ID,
	ord.employeeID,
	ord.date,
	ord.discount_cardID
FROM Orders ord
WHERE EXISTS(
	SELECT ID FROM Employees WHERE username=SUSER_ID() and ord.id = ID
	) OR EXISTS(
		SELECT *
			FROM Employees emp
			JOIN RolesToEmployees rte
				ON rte.employeeID = emp.ID
			JOIN Roles r
				ON r.ID = rte.roleID
			WHERE r.name = 'admin' and emp.username = SUSER_NAME()	
	);