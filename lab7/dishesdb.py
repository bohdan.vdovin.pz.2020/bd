import sys
import os

sys.path.append(os.path.abspath(os.path.join('..')))
from util.mygenerators import *

if __name__ == "__main__":
    # Globals
    with open("fill.sql", "wt+") as fd:
        dishID_offset = 100
        ordrerID_offset = 1000
        employeeID_offset = 100
        discountCardsID_offset = 5000
        customersID_offset = 10000
        # Dishes

        dishes_gen = [
            AutoIncermentGenerator(dishID_offset),
            ChoiceGenerator(
                [
                    "Bowls",
                    "Carafe",
                    "Susan",
                    "Platters",
                    "Salad bowls",
                    "Cup",
                    "Tea cup",
                ]
            ),
            ChoiceGenerator(
                ["dinnerware"] * 100
                + ["drinkware"] * 10
                + ["bakeware"] * 10
                + ["serveware"]
                + ["cookware"]
            ),
            ChoiceGenerator(
                [
                    "bowls",
                    "carafe",
                    "susan",
                    "platters",
                    "salad bowls",
                    "cup",
                    "tea cup",
                ]
            ),
        ]

        # DishPrices
        dishePrices_gen = [
            AutoIncermentGenerator(dishID_offset),
            FloatNumberGenerator(start=3.0, end=25.0, precision=2),
            DateGenerator(
                start=datetime.datetime(year=2017, month=1, day=1),
                end=datetime.datetime(year=2021, month=1, day=1),
            ),
        ]

        # DishCounts
        disheCounts_gen = [
            AutoIncermentGenerator(dishID_offset),
            IntegerNumberGenerator(start=100, end=500),
            DateGenerator(
                start=datetime.datetime(year=2017, month=1, day=1),
                end=datetime.datetime(year=2021, month=1, day=1),
            ),
        ]

        # Descriptions
        descriptions_gen = [
            AutoIncermentGenerator(dishID_offset),
            ChoiceGenerator(["China", "Ukraine", "USA", "Germany"]),
            DateGenerator(
                start=datetime.datetime(year=2017, month=1, day=1),
                end=datetime.datetime(year=2021, month=1, day=1),
            ),
            FloatNumberGenerator(start=100, end=5000),
        ]

        # DishMaterials
        dishMateriasl_gen = [
            AutoIncermentGenerator(dishID_offset),
            ChoiceGenerator(
                ["bone china", "earthenware", "porcelain", "melamine", "stoneware"]
            ),
        ]

        # Orders
        orders_gen = [
            AutoIncermentGenerator(ordrerID_offset),
            AutoIncermentGenerator(employeeID_offset),
            DateGenerator(
                start=datetime.datetime(year=2018, month=1, day=1),
                end=datetime.datetime(year=2022, month=8, day=1),
            ),
            AutoIncermentGenerator(discountCardsID_offset),
        ]

        # OrderToDishes
        ordersToDishes_gen = [
            AutoIncermentGenerator(ordrerID_offset),
            AutoIncermentGenerator(dishID_offset),
            IntegerNumberGenerator(start=1, end=5),
        ]

        # Employees
        employees_gen = [
            AutoIncermentGenerator(employeeID_offset),
            NameGenerator(),
            FirstnameGenerator(),
            LastnameGenerator(),
            EmailGenerator(),
            PasswordHashGenerator(),
            IntegerNumberGenerator(start=20, end=50),
            PhoneGenerator(),
            DateGenerator(
                start=datetime.datetime(year=2018, month=1, day=1),
                end=datetime.datetime(year=2020, month=1, day=1),
            ),
            StaticNumberGenerator(10000.0),
        ]

        # DiscountCards
        discountCards_gen = [
            AutoIncermentGenerator(discountCardsID_offset),
            AutoIncermentGenerator(customersID_offset),
            StaticNumberGenerator(0.05),
        ]

        # Customers
        customers_gen = [
            AutoIncermentGenerator(customersID_offset),
            FirstnameGenerator(),
            LastnameGenerator(),
            EmailGenerator(),
            PhoneGenerator(),
        ]

        print(insert_into_table(name="Dishes", generators=dishes_gen, n=50), file=fd)
        print(
            insert_into_table(name="DishPrices", generators=dishePrices_gen, n=50),
            file=fd,
        )
        print(
            insert_into_table(name="DishCounts", generators=disheCounts_gen, n=50),
            file=fd,
        )
        print(
            insert_into_table(name="Descriptions", generators=descriptions_gen, n=50),
            file=fd,
        )
        print(
            insert_into_table(name="DishMaterials", generators=dishMateriasl_gen, n=50),
            file=fd,
        )
        print(
            insert_into_table(name="Employees", generators=employees_gen, n=50), file=fd
        )
        print(
            insert_into_table(name="Customers", generators=customers_gen, n=50), file=fd
        )
        print(
            insert_into_table(name="DiscountCards", generators=discountCards_gen, n=50),
            file=fd,
        )
        print(insert_into_table(name="Orders", generators=orders_gen, n=50), file=fd)
        print(
            insert_into_table(
                name="OrderToDishes", generators=ordersToDishes_gen, n=50
            ),
            file=fd,
        )
