CREATE VIEW vCustomers
	AS
	SELECT
		cus.cname,
		cus.city as ccity,
		cus.rating,
		sal.sname,
		sal.city as scity,
		sal.comm
	FROM dbo.Customers cus
	JOIN Salers sal
		ON cus.snum = sal.snum
	WHERE
		sal.sname = SUSER_SNAME();