USE vdovinIndd;

DECLARE @ID int,
		@name nvarchar(50),
		@category nvarchar(50),
		@serveware_type nvarchar(50),
		@date date,
		@price float,
		@count int,
		@country nvarchar(50),
		@description nvarchar(100),
		@creation_time date,
		@weight float,
		@material1 nvarchar(50),
		@material2 nvarchar(50)

SET @ID=1000;
SET	@name='Super cup';
SET @category='cup';
SET @serveware_type='dinnerware';
SET	@date='2018-05-10';
SET	@price=57;
SET	@count=99;
SET	@country='china';
SET	@description='A cup that can bring calamity';
SET	@creation_time='2010-05-1';
SET	@weight=350.0;
SET	@material1='porcelain';
SET	@material2='melamine';

SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRY
	BEGIN TRAN
		IF EXISTS (SELECT ID FROM Dishes WHERE ID=@ID)
		BEGIN
			RAISERROR(50007, 16, 1);
		END

		INSERT INTO Dishes
		(ID, category, serveware_type, name)
		VALUES
		(@ID, @category, @serveware_type, @name);

		INSERT INTO DishPrices
		(dishID, price, time)
		VALUES
		(@ID, @price, @date);

		INSERT INTO DishCounts
		(dishID, count, time)
		VALUES
		(@ID, @count, @date);

		INSERT INTO DishMaterials
		(dishID, material)
		VALUES
		(@ID, @material1),
		(@ID, @material2);

		INSERT INTO Descriptions
		(dishID, country, creation_time, weight)
		VALUES
		(@ID, @count, @creation_time, @weight);

		SELECT resource_type, resource_associated_entity_id, request_mode,request_status
		FROM sys.dm_tran_locks dml
		INNER JOIN sys.dm_tran_current_transaction dmt
			ON dml.request_owner_id = dmt.transaction_id;

	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH

SELECT * FROM DishInfo;