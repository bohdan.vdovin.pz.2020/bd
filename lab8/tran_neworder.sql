-- Create new order
USE vdovinIndd;

DECLARE @orderID int,
		@dishID1 int,
		@count1 int,
		@dishID2 int,
		@count2 int,
		@employeeID int,
		@date date,
		@discountcardId int

SET	@orderID = 100;
SET	@dishID1 = 100;
SET	@count1 = 101;
SET	@dishID2 = 101;
SET	@count2 = 11;
SET	@employeeID = 150;
SET	@date = '2019-01-10';
SET	@discountcardId = 5000;
	

BEGIN TRY
	BEGIN TRAN
		-- Check if order with such id does not exist
		IF EXISTS (
			SELECT * FROM Orders
			WHERE ID = @orderID
		)
			BEGIN
				RAISERROR(50002,16,1)
			END

		-- Check if discount card exist
		IF NOT EXISTS (
			SELECT * FROM DiscountCards
			WHERE ID = @discountcardId
		)
			BEGIN
				RAISERROR(50003,16,1)
			END

		-- Check if employee with such id already exist
		IF NOT EXISTS (
			SELECT * FROM Employees
			WHERE ID = @employeeID
		)
			BEGIN
				RAISERROR(50004,16,1)
			END

		-- Check if dishes exist
		IF NOT EXISTS (
			SELECT * FROM Dishes
			WHERE ID = @dishID1
		) OR NOT EXISTS (
			SELECT * FROM Dishes
			WHERE ID = @dishID2
		)
			BEGIN
				RAISERROR(50005,16,1)
			END

		-- Insert order
		INSERT INTO Orders
			(ID, employeeID, date, discount_cardID)
		VALUES
			(@orderID, @employeeID, @date, @discountcardId);

		INSERT INTO OrderToDishes
			(orderId, dishID, count)
		VALUES
			(@orderID, @dishID1, @count1),
			(@orderID, @dishID2, @count2);	
	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH

SELECT * FROM Orders;