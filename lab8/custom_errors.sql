USE vdovinIndd;

EXEC sp_addmessage 50001, 16, 'Customer already exists';
EXEC sp_addmessage 50002, 16, 'Order already exist';
EXEC sp_addmessage 50003, 16, 'Discount card with provided ID does not exist';
EXEC sp_addmessage 50004, 16, 'Employee with such ID does not exist';
EXEC sp_addmessage 50005, 16, 'Dishes with such ID does not exist';
EXEC sp_addmessage 50006, 16, 'Employee already exists';
EXEC sp_addmessage 50007, 16, 'Dish already exists';