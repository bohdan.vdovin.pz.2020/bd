-- New employee
USE vdovinIndd
DECLARE
	@ID int,
	@username NVARCHAR(50),
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@email NVARCHAR(320),
	@password_hash NVARCHAR(100),
	@age int,
	@phone NVARCHAR(20),
	@started_working date,
	@salary float

SET @ID=99;
SET @username='Christopher Mooneyy'
SET	@firstname='Katrinaa';
SET	@lastname='Taggart';
SET	@email='kkkbDOFKBhWogK@gmail.com';
SET	@password_hash='e9dedc78f71f7b3d3f2960297c275acdfa1dac25eac097f2523dde98d70abfee';
SET	@age=21;
SET	@phone='546-466-2660';
SET	@started_working='2018-02-14 02:16:11';
SET	@salary=100000.0;

BEGIN TRY
	BEGIN TRAN
		IF NOT EXISTS (
			SELECT *
			FROM Employees emp
			WHERE ID = @ID
		)
			BEGIN
				INSERT INTO Employees
				(ID, username, firstname, lastname, email, password_hash, age, phone, started_working, salary )
				VALUES
				(@ID, @username, @firstname, @lastname, @email, @password_hash, @age, @phone, @started_working, @salary )
			END
		ELSE
			BEGIN
				RAISERROR(50006,16,1)
			END
	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH

SELECT * FROM Employees;