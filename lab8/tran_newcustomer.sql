-- Create new customer
USE vdovinIndd
DECLARE
	@ID int,
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@email NVARCHAR(320),
	@phone NVARCHAR(20),
	@discountCardID int,
	@discount float

SET @ID=1001;
SET @firstname='Andrew';
SET @lastname='Fuctoon';
SET @email='adnrew.functoon@gmail.com';
SET @phone='0422137709';
SET @discountCardID=1001;
SET @discount=0.05;

BEGIN TRY
	BEGIN TRAN
		IF NOT EXISTS (
			SELECT cu.ID
			FROM Customers cu
			INNER JOIN DiscountCards dc 
				ON cu.ID = dc.customerID 
			WHERE cu.ID=@ID
		)
			BEGIN
				INSERT INTO Customers
				(ID, firstname, lastname, email, phone)
				VALUES
				(@ID, @firstname, @lastname, @email, @phone);

				INSERT INTO DiscountCards
				(ID, customerID, discount)
				VALUES
				(@discountCardID, @ID, @discount)
			END
		ELSE
			BEGIN
				RAISERROR(50001,16,1)
			END
	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH

SELECT * FROM Customers;
SELECT * FROM DiscountCards;