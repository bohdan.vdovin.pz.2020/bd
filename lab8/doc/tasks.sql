﻿USE tempdb;
GO
-- Вправа 1
INSERT INTO table1 (i,col1,col2)
VALUES (1,'First row','First row');
INSERT INTO table1 (i,col1,col2)
VALUES (2,NULL,'Second row');
INSERT INTO table1 (i,col1,col2)
VALUES (3,'Third row','Third row');


USE tempdb;
GO
SELECT i,col1,col2
FROM table1;-- Вправа 2USE tempdb;
GO
TRUNCATE TABLE table1;

USE tempdb;
GO
BEGIN TRAN
INSERT INTO table1 (i,col1,col2) VALUES (1,'First row','First row');
INSERT INTO table1 (i,col1,col2) VALUES (2,NULL,'Second row');
INSERT INTO table1 (i,col1,col2) VALUES (3,'Third row','Third row');
COMMIT TRAN;

USE tempdb;
GO
SELECT i,col1,col2
FROM table1;