using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void SelectEmployeesWithSalaryGreaterThan (SqlDouble salary)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT emp.firstname, emp.lastname, emp.age, emp.salary FROM Employees emp WHERE emp.salary > @salary";
        cmd.Parameters.AddWithValue("@salary",salary);
        SqlContext.Pipe.ExecuteAndSend(cmd);
    }
}
