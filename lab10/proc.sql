-- Procedures
CREATE OR ALTER PROCEDURE NewOrder (
        @orderID int,
		@dishID1 int,
		@count1 int,
		@dishID2 int,
		@count2 int,
		@employeeID int,
		@date date,
		@discountcardId int
    )
AS 
BEGIN
    BEGIN TRY
        BEGIN TRAN
            -- Check if order with such id does not exist
            IF EXISTS (
                SELECT * FROM Orders
                WHERE ID = @orderID
            )
                BEGIN
                    RAISERROR(50002,16,1)
                END

            -- Check if discount card exist
            IF NOT EXISTS (
                SELECT * FROM DiscountCards
                WHERE ID = @discountcardId
            )
                BEGIN
                    RAISERROR(50003,16,1)
                END

            -- Check if employee with such id already exist
            IF NOT EXISTS (
                SELECT * FROM Employees
                WHERE ID = @employeeID
            )
                BEGIN
                    RAISERROR(50004,16,1)
                END

            -- Check if dishes exist
            IF NOT EXISTS (
                SELECT * FROM Dishes
                WHERE ID = @dishID1
            ) OR NOT EXISTS (
                SELECT * FROM Dishes
                WHERE ID = @dishID2
            )
                BEGIN
                    RAISERROR(50005,16,1)
                END

            -- Insert order
            INSERT INTO Orders
                (ID, employeeID, date, discount_cardID)
            VALUES
                (@orderID, @employeeID, @date, @discountcardId);

            INSERT INTO OrderToDishes
                (orderId, dishID, count)
            VALUES
                (@orderID, @dishID1, @count1),
                (@orderID, @dishID2, @count2);	
        COMMIT TRAN
    END TRY
    BEGIN CATCH
        SELECT
            error_number() AS ErrorNumber,
            error_severity() AS ErrorSeverity,
            error_state() as ErrorState,
            error_procedure() as ErrorProcedure,
            error_line() as ErrorLine,
            error_message() as ErrorMessage;
        ROLLBACK TRAN
    END CATCH
END

-- next

CREATE OR ALTER PROCEDURE NewEmployee(
    @ID int,
	@username NVARCHAR(50),
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@email NVARCHAR(320),
	@password_hash NVARCHAR(100),
	@age int,
	@phone NVARCHAR(20),
	@started_working date,
	@salary float
)
AS
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF NOT EXISTS (
			SELECT *
			FROM Employees emp
			WHERE ID = @ID
		)
			BEGIN
				INSERT INTO Employees
				(ID, username, firstname, lastname, email, password_hash, age, phone, started_working, salary )
				VALUES
				(@ID, @username, @firstname, @lastname, @email, @password_hash, @age, @phone, @started_working, @salary )
			END
		ELSE
			BEGIN
				RAISERROR(50006,16,1)
			END
	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH
END

--

CREATE OR ALTER PROCEDURE NewDish (
        @ID int,
		@name nvarchar(50),
		@category nvarchar(50),
		@serveware_type nvarchar(50),
		@date date,
		@price float,
		@count int,
		@country nvarchar(50),
		@description nvarchar(100),
		@creation_time date,
		@weight float,
		@material1 nvarchar(50),
		@material2 nvarchar(50)
)
AS
BEGIN
BEGIN TRY
	BEGIN TRAN
		IF EXISTS (SELECT ID FROM Dishes WHERE ID=@ID)
		BEGIN
			RAISERROR(50007, 16, 1);
		END

		INSERT INTO Dishes
		(ID, category, serveware_type, name)
		VALUES
		(@ID, @category, @serveware_type, @name);

		INSERT INTO DishPrices
		(dishID, price, time)
		VALUES
		(@ID, @price, @date);

		INSERT INTO DishCounts
		(dishID, count, time)
		VALUES
		(@ID, @count, @date);

		INSERT INTO DishMaterials
		(dishID, material)
		VALUES
		(@ID, @material1),
		(@ID, @material2);

		INSERT INTO Descriptions
		(dishID, country, creation_time, weight)
		VALUES
		(@ID, @count, @creation_time, @weight);

		SELECT resource_type, resource_associated_entity_id, request_mode,request_status
		FROM sys.dm_tran_locks dml
		INNER JOIN sys.dm_tran_current_transaction dmt
			ON dml.request_owner_id = dmt.transaction_id;

	COMMIT TRAN
END TRY
BEGIN CATCH
	SELECT
		error_number() AS ErrorNumber,
		error_severity() AS ErrorSeverity,
		error_state() as ErrorState,
		error_procedure() as ErrorProcedure,
		error_line() as ErrorLine,
		error_message() as ErrorMessage;
	ROLLBACK TRAN
END CATCH
END

--

CREATE OR ALTER PROCEDURE NewCustomer (
    @ID int,
	@firstname NVARCHAR(50),
	@lastname NVARCHAR(50),
	@email NVARCHAR(320),
	@phone NVARCHAR(20),
	@discountCardID int,
	@discount float
)
AS
BEGIN
    BEGIN TRY
        BEGIN TRAN
            IF NOT EXISTS (
                SELECT cu.ID
                FROM Customers cu
                INNER JOIN DiscountCards dc 
                    ON cu.ID = dc.customerID 
                WHERE cu.ID=@ID
            )
                BEGIN
                    INSERT INTO Customers
                    (ID, firstname, lastname, email, phone)
                    VALUES
                    (@ID, @firstname, @lastname, @email, @phone);

                    INSERT INTO DiscountCards
                    (ID, customerID, discount)
                    VALUES
                    (@discountCardID, @ID, @discount)
                END
            ELSE
                BEGIN
                    RAISERROR(50001,16,1)
                END
        COMMIT TRAN
    END TRY
    BEGIN CATCH
        SELECT
            error_number() AS ErrorNumber,
            error_severity() AS ErrorSeverity,
            error_state() as ErrorState,
            error_procedure() as ErrorProcedure,
            error_line() as ErrorLine,
            error_message() as ErrorMessage;
        ROLLBACK TRAN
    END CATCH
END


-- Functions
CREATE OR ALTER FUNCTION MaxThree(@one int, @two int, @three int)
RETURNS int
BEGIN
	DECLARE @m int
    IF @one < @two
        BEGIN
            IF @two < @three
            BEGIN
                SET @m=@three
            END
            ELSE
            BEGIN
                SET @m=@two
            END
        END
    ELSE
        BEGIN
            IF @one < @three
            BEGIN
                SET @m=@three
            END
            ELSE
            BEGIN
                SET @m=@one
            END
        END
	RETURN @m
END

-- Triggers
CREATE OR ALTER TRIGGER LogNewEmployee
ON Employees
AFTER INSERT
AS
BEGIN
    INSERT INTO Logs (
        time, text
    ) SELECT 
        GETDATE(),
        'Employee id: ' + str(ins.id) 
    FROM INSERTED ins
END

CREATE TABLE Logs(
	time DATETIME NOT NULL,
	text TEXT
);

EXEC sp_configure 'clr enabled', 1;
RECONFIGURE;
EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;
EXEC sp_configure 'clr strict security', 0;
RECONFIGURE;
create assembly example from 'D:\univ\term5\bd\lab10\Proc.dll' with permission_set = safe;

CREATE PROCEDURE SelectEmployeesWithSalaryGreaterThan(@salary float)
AS external name example.StoredProcedures.SelectEmployeesWithSalaryGreaterThan;

exec SelectEmployeesWithSalaryGreaterThan 1000;