BEGIN TRAN
EXEC NewOrder 101, 100, 101, 101, 11, 150, '2019-01-10', 5000;

SELECT * FROM Orders;

EXEC NewEmployee 98, 'Christopher Mooneyy', 'Katrinaa', 'Katrinaa','kkkbDOFKBhWogK@gmail.com', 'e9dedc78f71f7b3d3f2960297c275acdfa1dac25eac097f2523dde98d70abfee', 21, '546-466-2660', '2018-02-14 02:16:11', 5000.0;
SELECT * FROM Employees;

EXEC NewDish 1001, 'Super cup', 'cup', 'dinnerware', '2018-05-10', 57, 99, 'china', 'A cup that can bring calamity', '2010-05-1', '350.0', 'porcelain', 'melamine';
SELECT * FROM DishInfo;

EXEC NewCustomer 1002, 'Andrew', 'Fuctoon', 'adnrew.functoon@gmail.com', '0422137709', 1002, 0.05;

SELECT * FROM Customers;
SELECT * FROM DiscountCards;

DECLARE @res int;
EXEC @res = MaxThree 1100, 12, 1000;
SELECT @res

ROLLBACK TRAN