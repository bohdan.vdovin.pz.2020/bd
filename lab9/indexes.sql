CREATE CLUSTERED INDEX CLIDX_OrderToDishes_ID ON OrderToDishes(dishId);
DROP INDEX CLIDX_OrderToDishes_ID ON OrderToDishes;

CREATE INDEX NCLIX_OrderToDishes ON OrderToDishes(orderId);
DROP INDEX NCLIX_OrderToDishes;

CREATE INDEX NCLIX_Dishes_Name ON Dishes(name);
DROP INDEX NCLIX_Dishes_Name;

CREATE INDEX NCLIX_EmployeeName ON Employees(firstname, lastname);
DROP INDEX NCLIX_EmployeeName;

CREATE INDEX NCLIX_CustomerName ON Customers(firstname, lastname);
DROP INDEX NCLIX_CustomerName;