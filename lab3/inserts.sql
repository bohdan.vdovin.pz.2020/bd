-- Insert 1. Insert one entry
USE [vdovinInd]
INSERT INTO Customers
(
    [ID],
    [name],
    [email],
    [phone]
)
VALUES
(
    51,
    'Tomas Anderson',
    'tomas.adnerson@gmail.com',
    '11111111111'
);

-- Insert 2. several entries at once
USE [vdovinInd]
INSERT INTO Customers
(
    [ID],
    [name],
    [email],
    [phone]
)
VALUES
( 
    52,
    'Tomas Gedsbi',
    'tomas.gedsbi@gmail.com',
    '323402834'
),
(
    53,
    'Bonapard Gedsbi',
    'bonapard.gedsbi@gmail.com',
    '132131239'
);

-- Insert 3. change field order
USE [vdovinInd]
INSERT INTO Dishes
(
    [name],
    [category],
    [serveware_type],
    [ID]
)
VALUES
(
    'The cup of the Zador',
    'tea cup',
    'dinnerware',
    51
);

-- Default time
INSERT INTO DishPrices
(
    [price],
    [dishID],
	[time]
)
VALUES
( 25.5, 51, DEFAULT);

-- Default time
INSERT INTO DishCounts
(
    [count],
    [dishID]
)
VALUES
( 100, 51);

INSERT INTO DishMaterials
(
    [material],
    [dishID]
)
VALUES
( 'ceramic', 51),
( 'blackwood', 51);

INSERT INTO Descriptions
(
    [dishID],
    [weight],
    [country],
    [creation_time]
)
VALUES
(
    51,
    550.0,
    'China',
    '2019-01-23 10:04:10'
);

-- Insert4. Without field specification 
USE [vdovinInd]
INSERT INTO Employees
VALUES
(
    51,
    'Roman Stuck',
    'romanchuk.zalun.stuck@gmail.com',
    '3c4912ed945a0db1fb39df2face8fe0df2cdf5c07d383dbeb5f5ddb5631ea549',
    25,
    380983452512,
    '2018-07-03 22:50:09',
    10000.0
);