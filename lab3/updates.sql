-- Update 1.
UPDATE Descriptions
SET
    country = 'german',
    weight += 50.0
Where
    dishID = 100;

-- Update 2.
UPDATE Descriptions
SET
    weight += 100.0
WHERE 
    country = 'china'
    AND creation_time < '01.01.2020 00:00:00';

-- Update 3.
UPDATE Employees
SET
    email = 'rotokun.tero34@gmail.com', 
    password_hash = '90846de090637eed52d2cd8009504d25d220508c0b295e965a1d6878e612fa38'
WHERE
    ID = 100;

-- Update 4. With several subqueries
UPDATE Descriptions
SET
    country = (
        SELECT country FROM Descriptions Where dishID = 120
    ),
    weight = (
        SELECT country FROM Descriptions Where dishID = 115
    )
WHERE
    dishID = 101;
