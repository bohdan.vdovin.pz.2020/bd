-- Delete 1:
DELETE FROM Customers WHERE [ID] = 51; 

-- Delete2. 
DELETE FROM Employees
    Where
    [age] < 25
    AND
    [ID] NOT IN 
    (
        SELECT DISTINCT [ID] from orders
    );

-- Delete3. Delete dish price greater than 100.0
-- if the is at least anoter one which is lesser than 100.0
USE [vdovinInd]
DELETE FROM DishPrices
    Where [price] > 100.0 AND
    dishID IN (
        SELECT dishID FROM DishPrices Where [price] <= 100.0
    );
