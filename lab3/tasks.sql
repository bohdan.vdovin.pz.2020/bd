﻿-- Завдання 1. Вставте запис в таблицю, використавши запити і команду UNION. 
USE [vdovinInd]
INSERT INTO Customers
(ID, name, email, phone)
SELECT 52, name, email, phone FROM Employees
WHERE ID = 119
UNION
SELECT 53, 'Tomas Good', email, phone FROM Employees
WHERE ID = 101;

-- Завдання 2. Сформуйте запит на оновлення до бази даних з використанням інструкції JOIN.
USE [VdovinTestdb]
UPDATE s
SET s.city = 'London' 
FROM Salers AS s
JOIN (
        SELECT c.snum AS snum FROM Customers AS c WHERE c.rating < 100 
    UNION
        SELECT o.snum AS snum FROM Orders AS o WHERE o.amt = 18.69
) AS ot ON ot.snum = s.snum;

-- Завдання 3. Сформуйте запит на оновлення до бази даних з використанням у 
-- вкладеному підзапиті, як результуючого набору, кілька атрибутів таблиці. 
USE [vdovinInd]
UPDATE Customers
    SET 
	name = (SELECT TOP(1) name  FROM Employees),
	email = (SELECT TOP(1) email FROM Employees)
WHERE ID = 56;

-- Завдання 4. Перевірте чи можливе використання разом з інструкцією DELETE 
-- інструкції JOIN. Якими іншими командами можна замінити JOIN. Наведіть приклад. 
USE [VdovinTestdb]

DELETE s
FROM Salers AS s
JOIN Orders AS o
ON o.snum = s.snum
WHERE s.snum IS NULL; 

-- Other example without join
DELETE s
FROM Salers AS s
Where s.snum NOT IN (
    SELECT o.snum FROM Orders AS o Where o.snum = s.snum 
);
